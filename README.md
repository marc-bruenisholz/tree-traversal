# tree-traversal

Traverses a tree and calls preOrder() when visiting a node the first time
or postOrder() after a node and all its sub nodes have been visited.