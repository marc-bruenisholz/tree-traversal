<?php

namespace ch\_4thewin\TreeTraversal;

use ch\_4thewin\SqppTreeTraversalModels\NodeInterface;

class TreeTraversalExecution
{
    public function execute(TreeTraversalInterface $nodeVisitor, NodeInterface $tree, bool $visitRoot = false): void {
        $treeTraversal = new TreeTraversal($nodeVisitor);
        $treeTraversal->traverse($tree, $visitRoot);
    }
}