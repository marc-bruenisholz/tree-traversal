<?php

namespace ch\_4thewin\TreeTraversal;

use ch\_4thewin\SqppTreeTraversalModels\NodeInterface;

class TreeAdditionVisitor implements TreeTraversalInterface
{
    /**
     * @var NodeInterface[]
     */
    protected array $baseParentStack;

    protected TreeAdditionInterface $treeAddition;

    /**
     * @param TreeAdditionInterface $treeAddition
     * @param NodeInterface $baseTree
     */
    public function __construct(TreeAdditionInterface $treeAddition, NodeInterface $baseTree)
    {
        $this->treeAddition = $treeAddition;
        $this->baseParentStack = [$baseTree];
    }

    public function preOrder(NodeInterface $node, ?NodeInterface $parentNode, array $branch): bool
    {
        $baseParentNode = $this->baseParentStack[count($this->baseParentStack)-1];
        $baseChild = $this->treeAddition->add($baseParentNode, $node, $this->baseParentStack, $branch);
        // TODO Consider adding child here to guarantee it is added
        if($baseChild === null) {
            return false;
        }
        $this->baseParentStack[] = $baseChild;
        return true;
    }

    public function postOrder(NodeInterface $node, ?NodeInterface $parentNode, array $branch): void
    {
        array_pop($this->baseParentStack);
    }
}