<?php
/*
 * Copyright 2020 Marc Brünisholz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

namespace ch\_4thewin\TreeTraversal;

use ch\_4thewin\SqppTreeTraversalModels\NodeInterface;

/**
 * A class implementing this interface
 * can be used by TreeTraversal.
 * @package ch\_4thewin\TreeTraversal
 */
interface TreeTraversalInterface
{
    /**
     * Parents are visited first.
     * If false is returned, the subtree of
     * the given node is not traversed.
     * @param NodeInterface $node
     * @param NodeInterface|null $parentNode
     * @param NodeInterface[] $branch
     * @return bool
     */
    public function preOrder(NodeInterface $node, ?NodeInterface $parentNode, array $branch): bool;

    /**
     * Is called when all of a nodes children
     * were visited.
     * @param NodeInterface $node
     * @param NodeInterface|null $parentNode
     * @param NodeInterface[] $branch
     */
    public function postOrder(NodeInterface $node, ?NodeInterface $parentNode, array $branch): void;
}