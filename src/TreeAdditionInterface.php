<?php

namespace ch\_4thewin\TreeTraversal;

use ch\_4thewin\SqppTreeTraversalModels\NodeInterface;

interface TreeAdditionInterface
{
    /**
     * @param NodeInterface $baseParentNode
     * @param NodeInterface $templateNode
     * @param NodeInterface[] $baseBranch
     * @param NodeInterface[] $templateBranch
     * @return NodeInterface
     */
      function add(NodeInterface $baseParentNode, NodeInterface $templateNode, array $baseBranch, array $templateBranch): ?NodeInterface;
}