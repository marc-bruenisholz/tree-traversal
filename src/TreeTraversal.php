<?php
/*
 * Copyright 2020 Marc Brünisholz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

namespace ch\_4thewin\TreeTraversal;

use ch\_4thewin\SqppTreeTraversalModels\NodeInterface;

/**
 * Calls preOrder and postOrder methods on the
 * visitor class while traversing a tree.
 * @package ch\_4thewin\TreeTraversal
 */
class TreeTraversal
{
    private array $nodeStack;

    // Is used to keep the node on
    // the stack but marking it for removal
    // and postOrder call on second visit
    private array $visitedStack;

    /** @var TreeTraversalInterface */
    private TreeTraversalInterface $nodeVisitor;

    /**
     * @param TreeTraversalInterface $nodeVisitor
     */
    public function __construct(TreeTraversalInterface $nodeVisitor)
    {
        $this->nodeVisitor = $nodeVisitor;
    }

    private function addNodesInReverseToStack(
        array $nodes
    ) {
        // push to stack in reverse order so that first child
        // lands on top of stack and comes next
        for ($i = count($nodes) - 1; $i >= 0; $i--) {
            $this->nodeStack[] = $nodes[$i];
            $this->visitedStack[] = false;
        }
    }

    /**
     * Traverses the given nodes and calls
     * preOrder and postOrder methods of the given
     * class that implements NodeVisitorInterface.
     * @param NodeInterface $tree
     * @param bool $visitRoot
     */
    public function traverse(NodeInterface $tree, bool $visitRoot = false): void
    {
        $this->nodeStack = [];
        $this->visitedStack = [];
        $parentStack = [$tree];

        if($visitRoot) {
            $this->nodeVisitor->preOrder($tree, null, $parentStack);
        }

        $this->addNodesInReverseToStack($tree->getSubNodes());

        while (count($this->nodeStack) > 0) {
            $currentNode = $this->nodeStack[count($this->nodeStack) - 1];

            if ($this->visitedStack[count($this->nodeStack) - 1]) {
                $this->nodeVisitor->postOrder($currentNode, $parentStack[count($parentStack)-2], $parentStack);
                array_pop($parentStack);

                array_pop($this->nodeStack);
                array_pop($this->visitedStack);
            } else {
                $this->visitedStack[count($this->nodeStack) - 1] = true;
                $parentStack[] = $currentNode;

                if ($this->nodeVisitor->preOrder($currentNode, $parentStack[count($parentStack)-2], $parentStack)) {
                    $this->addNodesInReverseToStack($currentNode->getSubNodes());
                }
            }
        }

        if($visitRoot) {
            $this->nodeVisitor->postOrder($tree, null, $parentStack);
        }
    }
}