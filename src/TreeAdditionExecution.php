<?php

namespace ch\_4thewin\TreeTraversal;

use ch\_4thewin\SqppTreeTraversalModels\NodeInterface;

class TreeAdditionExecution
{
    /**
     * @param TreeAdditionInterface $treeAddition
     * @param NodeInterface $baseTree
     * @param NodeInterface $treeToAdd
     */
    public function execute(TreeAdditionInterface $treeAddition, NodeInterface $baseTree, NodeInterface $treeToAdd): void {
        $nodeVisitor = new TreeAdditionVisitor($treeAddition, $baseTree);
        $treeTraversal = new TreeTraversal($nodeVisitor);
        $treeTraversal->traverse($treeToAdd);
    }
}