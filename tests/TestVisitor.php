<?php
/*
 * Copyright 2020 Marc Brünisholz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

namespace ch\_4thewin\TreeTraversal;


use ch\_4thewin\SqppTreeTraversalModels\Node;
use ch\_4thewin\SqppTreeTraversalModels\NodeInterface;

class TestVisitor implements TreeTraversalInterface
{

    protected $events = [];
    protected $ignoredSubTree = [];

    public function addIgnoredSubTree(string $nodeName) {
        $this->ignoredSubTree[] = $nodeName;
    }

    /**
     * @inheritDoc
     */
    public function preOrder(NodeInterface $node, ?NodeInterface $parentNode, array $branch): bool
    {
        /** @var Node $testNode */
        $testNode = $node;
        $names = array_map(function (Node $e){return $e->getName();}, $branch);
        $this->events[] = 'preOrder ' . $testNode->getName() . ' ' . $parentNode->getName() . ' ' . implode('.', $names);
        return !in_array($testNode->getName(), $this->ignoredSubTree);
    }

    /**
     * @inheritDoc
     */
    public function postOrder(NodeInterface $node, ?NodeInterface $parentNode, array $branch): void
    {
        /** @var Node $testNode */
        $testNode = $node;
        $names = array_map(function (Node $e){return $e->getName();}, $branch);
        $this->events[] = 'postOrder ' . $testNode->getName() . ' ' . $parentNode->getName() . ' ' . implode('.', $names);
    }

    /**
     * @return array
     */
    public function getEvents(): array
    {
        return $this->events;
    }

    /**
     * @param array $events
     */
    public function setEvents(array $events): void
    {
        $this->events = $events;
    }


}