<?php

namespace ch\_4thewin\TreeTraversal;

use ch\_4thewin\SqppTreeTraversalModels\Node;
use ch\_4thewin\SqppTreeTraversalModels\NodeInterface;

class TestTreeAddition implements TreeAdditionInterface
{
    function add(NodeInterface $baseParentNode, NodeInterface $templateNode, array $baseBranch, array $templateBranch): ?NodeInterface
    {
        $subNodes = $baseParentNode->getSubNodes();
        $matchingSubNode = null;
        /** @var Node $subNode */
        foreach($subNodes as $subNode) {
            /** @var Node $templateNode */
            if($subNode->getName() === $templateNode->getName()) {
                $matchingSubNode =  $subNode;
            }
        }
        if($matchingSubNode === null) {
            /** @var Node $templateNode */
            $newNode =  new Node($templateNode->getName());
            $baseParentNode->addSubNode($newNode);
            return $newNode;
        }
        return $matchingSubNode;
    }
}