<?php
/*
 * Copyright 2020 Marc Brünisholz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

namespace ch\_4thewin\TreeTraversal;

use ch\_4thewin\SqppTreeTraversalModels\Node;
use PHPUnit\Framework\TestCase;

class TreeTraversalTest extends TestCase
{
    public function testAdditiveMerging() {
        $baseTree = (new Node('root'))
            ->addSubNode(
                (new Node('2'))
                    ->addSubNode(new Node('4'))
                    ->addSubNode(new Node('5'))
            )
            ->addSubNode(
                (new Node('3'))
            );

        $treeToAdd = (new Node('root'))
            ->addSubNode(
                (new Node('3'))
                    ->addSubNode(
                        (new Node('6'))
                            ->addSubNode(new Node('7'))
                            ->addSubNode(new Node('8'))
                            ->addSubNode(new Node('9'))
                    )
            );

        $expectedTree = (new Node('root'))
            ->addSubNode(
                (new Node('2'))
                    ->addSubNode(new Node('4'))
                    ->addSubNode(new Node('5'))
            )
            ->addSubNode(
                (new Node('3'))
                    ->addSubNode(
                        (new Node('6'))
                            ->addSubNode(new Node('7'))
                            ->addSubNode(new Node('8'))
                            ->addSubNode(new Node('9'))
                    )
            );
        $testTreeAdder = new TestTreeAddition();
        $treeAddition = new TreeAdditionExecution();
        $treeAddition->execute($testTreeAdder, $baseTree, $treeToAdd);

        self::assertEquals($expectedTree, $baseTree);
    }

    public function test()
    {
        $tree = (new Node('root'))
            ->addSubNode(
                (new Node('2'))
                    ->addSubNode(new Node('4'))
                    ->addSubNode(new Node('5'))
            )
            ->addSubNode(
                (new Node('3'))
                    ->addSubNode(
                        (new Node('6'))
                            ->addSubNode(new Node('7'))
                            ->addSubNode(new Node('8'))
                            ->addSubNode(new Node('9'))
                    )
            );
        $visitor = new TestVisitor();
        $treeTraversal = new TreeTraversal($visitor);
        $treeTraversal->traverse($tree);
        self::assertEquals(
            [
                'preOrder 2 root root.2',
                'preOrder 4 2 root.2.4',
                'postOrder 4 2 root.2.4',
                'preOrder 5 2 root.2.5',
                'postOrder 5 2 root.2.5',
                'postOrder 2 root root.2',
                'preOrder 3 root root.3',
                'preOrder 6 3 root.3.6',
                'preOrder 7 6 root.3.6.7',
                'postOrder 7 6 root.3.6.7',
                'preOrder 8 6 root.3.6.8',
                'postOrder 8 6 root.3.6.8',
                'preOrder 9 6 root.3.6.9',
                'postOrder 9 6 root.3.6.9',
                'postOrder 6 3 root.3.6',
                'postOrder 3 root root.3',
            ],
            $visitor->getEvents()
        );

        $visitor = new TestVisitor();
        $treeTraversal = new TreeTraversal($visitor);
        $visitor->addIgnoredSubTree('6');
        $treeTraversal->traverse($tree);
        self::assertEquals(
            [
                'preOrder 2 root root.2',
                'preOrder 4 2 root.2.4',
                'postOrder 4 2 root.2.4',
                'preOrder 5 2 root.2.5',
                'postOrder 5 2 root.2.5',
                'postOrder 2 root root.2',
                'preOrder 3 root root.3',
                'preOrder 6 3 root.3.6',
                'postOrder 6 3 root.3.6',
                'postOrder 3 root root.3',
            ],
            $visitor->getEvents()
        );
    }

}
